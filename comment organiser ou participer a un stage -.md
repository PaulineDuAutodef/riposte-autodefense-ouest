---
title: "Comment organiser ou participer à un stage ?"
order: 1
in_menu: true
---
Un stage Riposte dure 2 jours : différentes formules sont possibles : 
- deux journées à suivre
- deux journées séparées d'une semaine (deux samedis à suivre par exemple)
- en 4 ou 5 demi-journées (5 après-midis d'une même semaine, ou 4 mercredis matins à suivre par exemple)

Ces différentes formules permettent de s'adapter aux contraintes des différents publics (disponibilité, fatigue...).
Nous proposons également des animations plus courtes (de 1h30 à une demi-journée) pour découvrir l'autodéfense avant de s'engager sur un stage. 

Les stages sont accessibles à partir de 16 ans, et sont adaptés quel que soit son âge et sa condition physique. Pour les stages pour les adolescentes, [rendez-vous en page contacts pour d'autres ressources !](https://riposte-autodefense-ouest-paulineduautodef-fa95a8b93a04e3e2831a.gitlab.io/contact.html)

![Un groupe de femmes d'âges divers assises en cercle]({% link images/02-f-72.png %})

### Vous voulez participer à un stage 

- Voir le [calendrier des prochains stages dans l'Ouest](https://riposte-autodefense-ouest-paulineduautodef-fa95a8b93a04e3e2831a.gitlab.io/les%20stages%20dans%20l'ouest.html) 

-S'inscrire à la liste de diffusion pour être tenue au courant des prochains stages : [inscription liste](https://riposteouest.substack.com/)

- Écouter d'anciennes participantes qui témoignent du stage et de ses effets (podcast de 10 minutes) : [podcast De Vive Voix Riposte](https://www.nous-vous-ille.fr/devivevoix-riposte) 

### Vous voulez organiser un stage 

- Vous avez envie d'organiser un stage au sein de votre structure (association, entreprise, maison de quartier, centre socio-culturel...) : [contactez-nous !](https://riposte-autodefense-ouest-paulineduautodef-fa95a8b93a04e3e2831a.gitlab.io/contact.html) Nous animons des stages à partir de 10 personnes et jusqu'à 15 personnes. 

- Les stages et interventions peuvent s'adresser à toutes, ou à des personnes partageant un même vécu (violences conjugales, violences racistes, contexte professionnel...).

- L'organisation de stages au titre de la formation professionnelle continue est également possible.

- Des stages adaptés pour les participantes sourdes et malentendantes, ou en situation de handicap physique et maladies chroniques invalidantes, ou aveugles et malvoyantes ou en situation de handicap cognitif sont possibles avec [nos collègues de l'ARCAF : retrouvez leurs contacts ici](https://assoarcaf.wordpress.com/)

![Une femme en fauteuil roulant frappe avec force dans un coussin tenu par une autre femme]({% link images/04-f-72.png %}) 