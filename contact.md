---
title: "Contact"
order: 3
in_menu: true
---
#### **Pour des interventions en Pays de la Loire**  et en **Bretagne** Ille-et-Vilaine, Morbihan, Côtes-d'Armor

Pour nous contacter toutes les deux : contact@riposte-ouest.fr

**Pauline :** pauline.autodefense@lilo.org   
07 49 59 93 32 (Joignable du lundi 14h au vendredi 12h30)

Pauline anime des stages
- « tout public »
- Femmes victimes de violences conjugales
- Milieu professionnel / stage au titre de la formation professionnelle continue


**Meryem :** meryem.autodefense@protonmail.com   
06 86 63 62 31

Meryem anime des stages
- « tout public »
- Milieu festif
- Milieu professionnel
- Femmes racisées
- Femmes victimes de violences conjugales




#### **Nos collègues en Bretagne** Finistère, Morbihan, Côtes-d'Armor

**Joun** : adf.joun@gmail.com

Joun anime des stages : 
- « tout public »
- Milieu professionnel 
- Milieu festif
- Femmes et personnes migrantes et/ou racisées
- Personnes LGBTQIA+
- Femmes victimes de violences conjugales
- Stages adolescent.es (12-14 ans ou 14-16 ans)
- Stages enfants (10-12 ans)


**Zila** : cj-autodefense@riseup.net 
06 15 41 81 78 (Joignable les mardis et mercredis)

Zila anime des stages : 
- « tout public »
- Femmes victimes de violences conjugales
- Milieu professionnel
- Milieu festif
- Personnes LGBTQIA+
- Stages adolescentes (12-14 ans ou 14-16 ans)
- Stages enfants (8-10 ans ou 10-12 ans) 
- Zila anime également ces stages en breton et espagnol 



_Crédit illustrations : Édith Kurosaka_ 