---
title: "Les stages dans l'Ouest"
order: 2
in_menu: true
---
#### Les prochains stages Riposte en inscription libre dans l'Ouest : 

(Contactez directement les structures qui organisent les stages pour vous inscrire)  

**Stage à Bruz (35) avec le Centre social L'Escale les 29 et 30 mars 2025** Pour s'inscrire (stage gratuit pour les participantes. Priorité aux habitantes de Bruz puis aux habitantes de Rennes Métropole) : Charlotte, animatrice du Centre social : animation@escale-bruz.fr ou 06 12 63 76 95

**Stage à Nantes (44) pour les femmes Sourdes, animé en LSF par une animatrice Sourde, les 5 et 6 avril 2025**  
Informations [sur le site de l'ARCAF](https://assoarcaf.wordpress.com/riposte/stages-riposte-en-lsf/)  
ou par mail : arca-f@riseup.net  
et [inscription via Hello Asso](https://www.helloasso.com/associations/association-arcaf-autodefense-et-ressources-pour-le-choix-et-l-autonomie-des-femmes/evenements/stage-riposte-en-lsf-a-nantes)

**Stages à venir en Maine-et-Loire avec l'association Corps à Cœur en juin et septembre 2025**. Pour être tenue au courant et s'inscrire : corpsacoeur49@gmail.com

![Affiche événements 8 mars à Bruz : dessin d'un groupe de femmes toutes différentes levant le poing]({% link images/Affiche-A3-droits-femmes-2025-petite.png %})

#### Autres stages d'autodéfense en inscription libre dans l'Ouest : 
Pas de dates fixées à ce jour. 

#### Autres stages en France : 
- [Stages Riposte en Ile-de-France](https://assoarcaf.wordpress.com/riposte/stages/)

- [Stages Riposte à Marseille et en Provence-Alpes-Côte d'Azur](https://assoarcaf.wordpress.com/riposte/stages-a-marseille-et-paca/)

- [Autres associations et animateurices d'autodéfense en France, en Belgique et en Suisse](https://assoarcaf.wordpress.com/liens/) 