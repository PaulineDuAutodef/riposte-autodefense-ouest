---
title: "Accueil"
order: 0
in_menu: true
---
# Riposte - autodéfense pour les femmes

![4 femmes de profil]({% link images/Riposte-affiche-extrait.png %})

Nous proposons des stages d'autodéfense pour les femmes* dans l'Ouest de la France. 
Le but de l'autodéfense est de prévenir les agressions. Cela passe par acquérir les moyens de se défendre, de prendre conscience de sa force et de (re)prendre confiance en soi. 

Cette forme d’autodéfense est adaptée à toutes, quel que soit son âge et sa condition physique. Nos interventions prennent en compte les différents vécus des participantes et les violences spécifiques qui sont liés à l'origine, l'orientation sexuelle, la religion, l'identité de genre, les capacités physiques, la couleur de peau, l'âge... 



### Ce qui est abordé dans nos interventions : 
- Apprendre à reconnaître différents types d'agression

- Découvrir et pratiquer des stratégies d'autodéfense physique et verbale

- S'outiller pour réagir en cas d'agression par des personnes inconnues, connues ou proches

- Identifier ses limites et les manières de les faire respecter
- Se reconnecter à sa confiance en soi et à ses ressources

Pour en savoir plus sur Riposte : [association ARCAF](https://assoarcaf.wordpress.com/riposte/){:target="_blank" }

Envie de [participer ou d'organiser un stage ? C'est par ici](https://riposte-autodefense-ouest-paulineduautodef-fa95a8b93a04e3e2831a.gitlab.io/comment%20organiser%20ou%20participer%20a%20un%20stage%20-.html) 

Pour [nous contacter : c'est par là](https://riposte-autodefense-ouest-paulineduautodef-fa95a8b93a04e3e2831a.gitlab.io/contact.html) 

_*Les interventions Riposte sont pensées pour les femmes (cis et trans) et les personnes perçues, assignées femmes, en tant que groupe ciblé par des agressions spécifiques. Une question à ce sujet ? Vous vous demandez si le stage est adapté à vous ou à une personne que vous connaissez ? Prenez contact avec nous !_ 